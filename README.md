Flowee the Hub repository
=========================

![Release pipeline status](https://gitlab.com/FloweeTheHub/thehub/badges/master/pipeline.svg)


# This is theHub repo with Flowee libs and some server apps to get the most out of Bitcoin Cash (BCH)

See more details on https://flowee.org/about/


This repository contains

* the Hub: the headless (server) Bitcoin Cash node software
* Indexer: Provides various data-stores to speed up lookups on the blockchain.
* Libraries shared between many of the Flowee applications.
* hub-cli: the command-line interface to the Hub server.
* bitcore-proxy: a client of hub+indexer to generate the bitcore APIs
* txVulcano: Transaction generator testing application.
* unspentdb: Application to inspect and optimize your UTXO database.

# Building a Hub that connects you to Bitcoin Cash

Bitcoin Cash is still a mysterious black box to most people and companies
that would potentially want to use it, or build applications with.

Flowee has been built to help you use Bitcoin Cash. Bring you a platform to
stand on when you simply use or when you build with Bitcoin Cash.

The codebase is derived from the one that originally was created by Satoshi
Nakamoto, the inventor of Bitcoin. This way you know you won't get
compatibility issues. Flowee is adjusted for greatly increased speed and
reliability as well as for scaling up to much larger blocks than the
competition.

# Installation

**To compile and install Flowee** on Ubuntu, install the dependencies

`sudo apt install libssl-dev libevent-dev libboost-all-dev libminiupnpc-dev qt5-default libprotobuf-dev pkgconf`

To compile and install Flowee on MacOS, install the dependencies

`brew install cmake libevent boost miniupnpc qt pkg-config`

Then clone the repo and use cmake to create the makefile

```
mkdir thehub/build
cd thehub/build
cmake CMakeLists.txt ..
make
make install
```

# Links

* Upstream: https://codeberg.org/Flowee/thehub
* Website: https://flowee.org
* Twitter: https://twitter.com/floweethehub
* Telegram: https://t.me/Flowee_org
