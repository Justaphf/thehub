/*
 * This file is part of the Flowee project
 * Copyright (C) 2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <common/TestFloweeBase.h>

class TestAddrMan : public TestFloweeBase
{
    Q_OBJECT
private slots:
    void read();
    void readCorrupted();

    void simple();
    void ports();
    void select();
    void new_collisions();
    void tried_collisions();
    void serialization();
};

#include <addrman.h>
class CDataStream;

class AddrManMock : public CAddrMan
{
public:
    //! Ensure that bucket placement is always the same for testing purposes.
    void makeDeterministic();
};

class AddrManSerializationMock : public AddrManMock
{
public:
    virtual void Serialize(CDataStream& s, int nType, int nVersionDummy) const = 0;
};

class AddrManUncorrupted : public AddrManSerializationMock
{
public:
    void Serialize(CDataStream& s, int nType, int nVersionDummy) const;
};

class AddrManCorrupted : public AddrManSerializationMock
{
public:
    void Serialize(CDataStream& s, int nType, int nVersionDummy) const;
};
