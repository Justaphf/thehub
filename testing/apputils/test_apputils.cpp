/*
 * This file is part of the Flowee project
 * Copyright (C) 2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "test_apputils.h"
#include <Logger.h>

#include <apputils/Mnemonic.h>
#include <utils/HDMasterKey.h>
#include <cashaddr.h>

void TestAppUtils::mnemonics()
{
    QFETCH(QString, mnemonic);  // n-words of seed phrase.
    QFETCH(int, validity);      // MnemonicException:Error
    QFETCH(int, errorPos);

    Mnemonic m;
    m.registerWordList("english",
            QString("%1/english.txt").arg(SRCDIR));
    int pos = -2;
    auto rc = m.validateMnemonic(mnemonic, pos);
    QCOMPARE(rc, validity);
    QCOMPARE(pos, errorPos);
}

void TestAppUtils::mnemonics_data()
{
    QTest::addColumn<QString>("mnemonic");  // n-words of seed phrase.
    QTest::addColumn<int>("validity");      // Mnemonic::Validity
    QTest::addColumn<int>("errorPos");

    QTest::newRow("small-zero")
            << "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("small-mid")
            << "legal winner thank year wave sausage worth useful legal winner thank yellow"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("small-max")
            << "zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo wrong"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("mid-zero")
            << "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon agent"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("mid-mid")
            << "legal winner thank year wave sausage worth useful legal winner thank year wave sausage worth useful legal will"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("mid-other")
            << "letter advice cage absurd amount doctor acoustic avoid letter advice cage absurd amount doctor acoustic avoid letter always"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("mid-max")
            << "zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo when"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("long-min")
            << "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon art"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("hamster")
            << "hamster diagram private dutch cause delay private meat slide toddler razor book happy fancy gospel tennis maple dilemma loan word shrug inflict delay length"
            << (int) Mnemonic::Valid << -1;

    QTest::newRow("JP-zero")
            << "あいこくしん　あいこくしん　あいこくしん　あいこくしん　あいこくしん　あいこくしん　あいこくしん　"
               "あいこくしん　あいこくしん　あいこくしん　あいこくしん　あおぞら"
            << (int) Mnemonic::UnknownLanguage << 0;
    QTest::newRow("oneWord") << "hamster" << (int) Mnemonic::IncorrectWordCount << -2;
    QTest::newRow("tooLittle") << "a a a a a a a a" << (int) Mnemonic::IncorrectWordCount << -2;
    QTest::newRow("tooMany") << "a a a a a a a a a a a a a a a a a a a a a a a a a" << (int) Mnemonic::IncorrectWordCount << -2;
    QTest::newRow("3rd")
            << "zoo zoo foo zoo zoo zoo zoo zoo zoo zoo zoo wrong"
            << (int) Mnemonic::UnknownWord << 8;
    QTest::newRow("small-max")
            << "zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo abandon"
            << (int) Mnemonic::ChecksumFailure << -1;

    // test-vectors
    // https://github.com/bip32JP/bip32JP.github.io/raw/master/test_JP_BIP39.json
    // https://github.com/trezor/python-mnemonic/raw/master/vectors.json
}

void TestAppUtils::seedToMnemonic()
{
    QFETCH(QString, seed);      // hex encoded binary seed
    QFETCH(QString, mnemonic);  // n-words of seed phrase.

    QByteArray seedBytes = QByteArray::fromHex(seed.toLatin1());
    std::vector<uint8_t> seedVector(seedBytes.constData(), seedBytes.constData() + seedBytes.size());
    Mnemonic m;
    m.registerWordList("english",
            QString("%1/english.txt").arg(SRCDIR));


    QString result = m.generateMnemonic(seedVector, "english");
    QCOMPARE(result, mnemonic);
}

void TestAppUtils::seedToMnemonic_data()
{
    QTest::addColumn<QString>("seed");
    QTest::addColumn<QString>("mnemonic");      // n-words of seed phrase.

    QTest::newRow("small-zero")
            << "00000000000000000000000000000000"
            << "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about";
    QTest::newRow("small-mid")
            << "7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f"
            << "legal winner thank year wave sausage worth useful legal winner thank yellow";
    QTest::newRow("small-mid2")
            << "80808080808080808080808080808080"
            << "letter advice cage absurd amount doctor acoustic avoid letter advice cage above";
    QTest::newRow("small-top")
            << "ffffffffffffffffffffffffffffffff"
            << "zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo wrong";
    QTest::newRow("mid-zero")
            << "000000000000000000000000000000000000000000000000"
            << "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon agent";
    QTest::newRow("mid-mid")
            << "7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f"
            << "legal winner thank year wave sausage worth useful legal winner thank year wave sausage worth useful legal will";
    QTest::newRow("mid-mid2")
            << "808080808080808080808080808080808080808080808080"
            << "letter advice cage absurd amount doctor acoustic avoid letter advice cage absurd amount doctor acoustic avoid letter always";
    QTest::newRow("mid-top")
            << "ffffffffffffffffffffffffffffffffffffffffffffffff"
            << "zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo when";
    QTest::newRow("long-zero")
            << "0000000000000000000000000000000000000000000000000000000000000000"
            << "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon "
               "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon art";
}

QTEST_MAIN(TestAppUtils)
