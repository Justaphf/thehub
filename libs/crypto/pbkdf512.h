/*
 * This file is part of the Flowee project
 * Copyright (c) 2008 Damien Bergamini <damien.bergamini@free.fr>
 * Copyright (C) 2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_PKCS5PBKDF512_H
#define FLOWEE_PKCS5PBKDF512_H

#include <stddef.h>
#include <stdint.h>

/* Password-Based Key Derivation Function 2 (PKCS #5 v2.0). */
/* Code based on IEEE Std 802.11-2007, Annex H.4.2. */
/* returns 0 if successful. */
int pbkdf512(const uint8_t* passphrase, size_t passphraseLength, const uint8_t *salt, size_t saltLength, uint8_t *key, size_t keyLength, size_t iterations);

#endif
