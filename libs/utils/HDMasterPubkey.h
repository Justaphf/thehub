/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HDMASTERPUBKEY_H
#define HDMASTERPUBKEY_H

#include "primitives/pubkey.h"

class HDMasterKey;

class HDMasterPubkey
{
public:
    /// Adding this value to a path-element makes it 'hardened'
    static const uint32_t Hardened = 0x80000000;

    HDMasterPubkey(const HDMasterPubkey &other) = default;

    /**
     * Convert a string based derivation path into a vector of fields.
     * This throws an exception when the string is not a fully valid derivation path.
     */
    static std::vector<uint32_t> deriveFromString(const std::string &path);

    /**
     * Create a master pubkey from a serialialized xpub
     * @see HDMasterKey::toXPubString()
     * @param xpub a base58 encoded xpub
     */
    static HDMasterPubkey fromXPub(const std::string &xpub);
    static HDMasterPubkey fromHDMaster(const HDMasterKey &key, const std::vector<uint32_t> &derivationPath);
    static HDMasterPubkey fromHDMaster(const HDMasterKey &key, const std::string &derivationPath);

    /**
     * Used in the string output methods.
     */
    enum Chain {
        Testnet,
        MainChain
    };
    /// returns true if the key is valid.
    bool isValid() const;

    std::string toString() const; ///< output xpub pubkey key as text

    /**
     * Derive a pubkey from this master key using a full path.
     * This class typically does not represent a HD-master key because of hardening,
     * this means that this method will skip deriving a number of levels that has
     * already been done by the HDMasterKey::toXPubString() method.
     *
     * Or, in other words, this method still takes the FULL derivation path because
     * we know how many steps to skip.
     */
    PublicKey derive(const std::vector<uint32_t> &path) const;
    /// Convenience method
    PublicKey derive(const std::string &path) const;

    HDMasterPubkey &operator=(const HDMasterPubkey &other) = default;
    friend bool operator==(const HDMasterPubkey &a, const HDMasterPubkey &b) {
        return memcmp(&a.m_fingerprint[0], &b.m_fingerprint[0], 4) == 0 && a.m_chaincode == b.m_chaincode && a.m_pubkey == b.m_pubkey;
    }

    Chain chain() const;

protected:
    HDMasterPubkey();

private:
    uint8_t m_fingerprint[4];
    ChainCode m_chaincode;
    PublicKey m_pubkey;
    Chain m_chain = MainChain;
    int m_depth = 0;
    uint32_t m_lastChild = 0;
};

#endif
